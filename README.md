# mykro/java8-jre
A build on top of Debian 8

## Image Sizes 
[![](https://badge.imagelayers.io/mykro/java8-jre:latest.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u72) 8u72, latest
[![](https://badge.imagelayers.io/mykro/java8-jre:latest.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u71) 8u71
[![](https://badge.imagelayers.io/mykro/java8-jre:latest.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u66) 8u66
[![](https://badge.imagelayers.io/mykro/java8-jre:8u51.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u51) 8u51
[![](https://badge.imagelayers.io/mykro/java8-jre:8u45.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u45) 8u45
[![](https://badge.imagelayers.io/mykro/java8-jre:8u40.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u40) 8u40
[![](https://badge.imagelayers.io/mykro/java8-jre:8u31.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u31) 8u31
[![](https://badge.imagelayers.io/mykro/java8-jre:8u25.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u25) 8u25
[![](https://badge.imagelayers.io/mykro/java8-jre:8u20.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u20) 8u20
[![](https://badge.imagelayers.io/mykro/java8-jre:8u11.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u11) 8u11
[![](https://badge.imagelayers.io/mykro/java8-jre:8u5.svg)](https://imagelayers.io/?images=mykro/java8-jre:8u5) 8u5
[![](https://badge.imagelayers.io/mykro/java8-jre:8.svg)](https://imagelayers.io/?images=mykro/java8-jre:8) 8

## Using this Image
Primarily, this image has been built to be a base for any Java Application that requires the Server JRE. It also strips out as much as possible to keep the image as lean as it can be.

```Dockerfile
FROM mykro/java8-jre:latest
MAINTAINER me <me@me.com>

# Build your java app container here

```